import express from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import indexRoutes from './routes/index.routes';
import postsRoutes from './routes/PostsRoutes';
import usersRoutes from './routes/UserRoutes';
import mongoose from 'mongoose';
import compression from 'compression';
import cors from 'cors';

class Server{

	private app: express.Application;

	constructor(){
		this.app = express();
		this.config();
		this.middlewares();
		this.routes();
	}

	config(){
		const MONGO_URI = 'mongodb://localhost/API-REST-Typescript-Mongo';
		mongoose.set('useFindAndModify',false);
		mongoose.set('useUnifiedTopology',true);
		mongoose.connect(MONGO_URI || process.env.MONGODB_URL,{
			useNewUrlParser: true,
			useCreateIndex: true
		})
		.then (db => console.log('DB is connected'));
		
		this.app.set('port', process.env.PORT || 3000);
	}


	middlewares(){
		this.app.use(morgan('dev'));
		this.app.use(express.json());
		this.app.use(express.urlencoded({extended: false}));
		this.app.use(helmet());
		this.app.use(compression());
		this.app.use(cors());
	}


	routes(){
		this.app.use(indexRoutes);
		this.app.use('/api/posts',postsRoutes);
		this.app.use('/api/users',usersRoutes);
	}

	async start(){
		await this.app.listen(this.app.get('port'),() => {
			console.log('Server on port',this.app.get('port'));
		});
	}
}

const server = new Server();
server.start();