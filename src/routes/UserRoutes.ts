import { Request, Response, Router } from 'express';
import User from '../models/User';

class UserRoutes{

	public router: Router;

	constructor(){
		this.router = Router();
		this.routes();
	}

	//Devuelve todas las publicaciones
	public async getUsers(req: Request, res: Response):Promise<void>{
		const user = await User.find();
		res.json(user);
	}

	//Devuelve una publicacion en especifico
	public async getUser(req: Request, res: Response):Promise<void>{
		const user = await User.findOne({ username: req.params.username }).populate('posts','title url -_id');
		res.json(user);
	}

	//Crea una publicacion
	public async createUser(req: Request, res: Response):Promise<void>{
		const newUser = new User(req.body);
		await newUser.save();
		res.json({data: newUser});
	}

	//Modificar una publicacion
	public async updateUser(req: Request, res: Response):Promise<void>{
		const { username } = req.params;
		const user = await User.findOneAndUpdate({username: username}, req.body, { new: true });
		res.json(user);
	}

	//Borrar una publicacion
	public async deleteUser(req: Request, res: Response):Promise<void>{
		const { username } = req.params;
		const user = await User.findOneAndDelete({username: username});
		res.json({response: 'Post Deleted Successfully'});	
	}

	routes(){
		this.router.get('/', this.getUsers);
		this.router.get('/:username', this.getUser);
		this.router.post('/', this.createUser);
		this.router.put('/:username',this.updateUser);
		this.router.delete('/:username',this.deleteUser);
	}
}

const userRoutes = new UserRoutes();
export default userRoutes.router;