import { Request, Response, Router } from 'express';

class IndexRoutes{

	public router: Router;

	constructor(){
		this.router = Router();
		this.routes();
	}

	routes(){
		this.router.get('/',(req, res)=> res.send('Hola'));	
	}
}

const indexRoutes = new IndexRoutes();
export default indexRoutes.router;